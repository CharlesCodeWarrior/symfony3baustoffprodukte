<?php
/**
 * Created by PhpStorm.
 * User: carlos
 * Date: 28.06.17
 * Time: 17:45
 */

namespace AppBundle\Controller;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\BaustoffProdukt;

class BaustoffProdukteImportController extends Controller
{
    /**
     * @Route("/BaustoffProdukteImport")
     */
    public function listAction()
    {


        $em = $this->getDoctrine()->getManager();
        $txImpProdukteArray = $em->getRepository(BaustoffProdukt::class)->findAll();
        return $this->render('baustoffProdukte/list.html.twig',
            ['txImpProdukteArray' => $txImpProdukteArray
            ]
        );
    }
}