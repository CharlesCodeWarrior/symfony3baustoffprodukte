<?php
/**
 * Created by PhpStorm.
 * User: carlos
 * Date: 28.06.17
 * Time: 17:45
 */

namespace AppBundle\Controller;



use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\BaustoffProdukt;
use AppBundle\Entity\Product;
use AppBundle\Entity\Category;


/**
 * this class goes through all imported Baustoff-Produkt Objects  and copies their values to a new Product-Object
 * Class BaustoffProduktToProductCopyController
 * @package AppBundle\Controller
 * @author Carlos Breling
 */

class BaustoffProduktToProductCopyController extends Controller
{
    /**
     * @Route("/products")
     */
    public function listAction()
    {

        $em = $this->getDoctrine()->getManager();
        //$txImpProdukteArray = $em->getRepository(BaustoffProdukt::class)->findAll();
        $maxAnzahl = 50;
        $txImpProdukteArray = $em->getRepository(BaustoffProdukt::class)->findBy(array(), array(), $maxAnzahl);
        $productsArrayOld = $em->getRepository(Product::class)->findAll();;
        $productsArray = [Product::class];
        if (isset($txImpProdukteArray) && $txImpProdukteArray != null && !empty($txImpProdukteArray)) {
//        if($txImpProdukteArray) {
            // relate this product to the category, TEST, mockup
            $category = new Category();
            $category->setName('Fliessentechnik 77');
            $category->setUidBaustoff(7777);

            //lese 2 Kategorien

            $em->persist($category);
            //go through all imported Baustoff-Produkte Objects and copy their values to a new product Object

            foreach ($txImpProdukteArray as $index => $tempBaustoffProdukt) {
                /**@var BaustoffProdukt $tempBaustoffProdukt */
                $tempBaustoffProdukt;
                /**@var Product $tempProduct */
                $tempProduct = new Product();
                $tempProduct->setName($tempBaustoffProdukt->getName());

                $tempBaustoffProdukt = randomizeBaustoffProdukt($tempBaustoffProdukt);
                // CAREFUL source data
                $em->persist($tempBaustoffProdukt);


                $tempProduct->setUidBaustoff($tempBaustoffProdukt->getUid());
                //todo: search for a Product with the same Uid, but in  $productsArrayOld only one db query,add: getProduct for uid Method, UPDATE the entry

                $tempProduct->setDescription($tempBaustoffProdukt->getBezeichnung());
                $tempProduct->setIntroText($tempBaustoffProdukt->getEingangstext());
                //#test echo ' Prod Name:'.$tempProduct->getName();


                $tempProduct->setCategories(array($category));

                $productsArray[$index] = $tempProduct;

                // tells Doctrine you want to (eventually) save the Product (no queries yet)


                $em->persist($tempProduct);

                //}
            }
            // actually executes the queries (i.e. the INSERT query)
            // of course after the loop. only one query
            $em->flush();

            return $this->render('products/list.html.twig',
                ['productsArray' => $productsArray
                ]
            );

            function randomizeBaustoffProdukt($tempBaustoffProdukt)
            {
                //if you want to randomize source table for anonymity. copy the table before! :-)
                $letter = chr(mt_rand(65, 90));
                $fictionalName = $letter . mt_rand(1, 100);
                $tempBaustoffProdukt->setName($fictionalName);
                $tempBaustoffProdukt->setBild($fictionalName . '.jpg');
                $textArray = ['Rostschutz-Grundierung – schützt Eisen und Stahl zuverlässig vor Korrosion Für den Innen- und Außenbereich ',
                    'Holzgrundierung - ist ein farbloser, wasserverdünnbarer Grundieranstrich mit feuchtigkeitsregulierender, wetterbeständiger und geruchsarmer Eigenschaft. Der Holzgrund ist für den Einsatz auf maßhaltigen Holzauteilen, wie z.B. Verkleidungen, Vertäfelungen, Fenster und Türen im Innen- und Außenbereich konzipiert.',
                    'Zement Grundierung - Zur Oberflächenverfestigung und Regulierung des Saugverhaltens auf saugenden UntergründenGrundierung für saugende Untergründe. Zur Oberflächenverfestigung und Regulierung des Saugverhaltens. Schnell trocknend, dampfdiffusionsoffen. Für optimalen Halt von Fliesenklebern, Spachtelmassen etc.'];
                $tempBaustoffProdukt->setEingangstext($textArray[mt_rand(0, 2)]);
                $beschreibungsArray = ['schützt Eisen und Stahl zuverlässig vor Korrosion Für den Innen- und Außenbereich',
                    'ist ein farbloser, wasserverdünnbarer Grundieranstrich mit feuchtigkeitsregulierender, wetterbeständiger und geruchsarmer Eigenschaft',
                    'Zur Oberflächenverfestigung und Regulierung des Saugverhaltens auf saugenden UntergründenGrundierung für saugende Untergründe'];
                $tempBaustoffProdukt->setBezeichnung($beschreibungsArray[mt_rand(0, 2)]);
                $eigenschaftenArray = [' - für innen und aussen
                    - Gebrauchsfertig
                    - Sehr hohes Eindringvermögen', '- Diffusionsoffen
                    - Oberflächenverfestigend
                    - Staubbindend', '- Saugfähigkeitregulierend
                    - Lösemittel- und weichmacherfrei, -emmisionsarm
                    - Verarbeitung mit Maschine oder von Hand'];
                $tempBaustoffProdukt->setEigenschaften($eigenschaftenArray[mt_rand(0, 2)]);

                return $tempBaustoffProdukt;
            }

        }


    }



}