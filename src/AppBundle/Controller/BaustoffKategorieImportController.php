<?php
/**
 * Created by PhpStorm.
 * User: carlos
 * Date: 28.06.17
 * Time: 17:45
 */

namespace AppBundle\Controller;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\BaustoffKategorie;

class BaustoffKategorieImportController extends Controller
{
    /**
     * @Route("/BaustoffKategorienImport")
     */
    public function listAction()
    {


        $em = $this->getDoctrine()->getManager();
        //Vorsicht Kategorien enthalten Unterkategorien. Dump würde extrem gross
        $txImpKategorienArray = $em->getRepository(BaustoffKategorie::class)->findAll();
        return $this->render('baustoffKategorien/list.html.twig',
            ['txImpKategorienArray' => $txImpKategorienArray
            ]
        );

    }
}