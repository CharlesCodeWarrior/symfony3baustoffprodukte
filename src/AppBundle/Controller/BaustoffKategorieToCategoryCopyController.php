<?php
/**
 * Created by PhpStorm.
 * User: carlos
 * Date: 28.06.17
 * Time: 17:45
 */

namespace AppBundle\Controller;



use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\BaustoffKategorie;
use AppBundle\Entity\Category;

/**
 * this class goes through all imported Baustoff-Kategorie Objects  and copies their values to a new Category-Object
 * Class BaustoffKategorieToCategoryCopyController
 * @package AppBundle\Controller
 * @author Carlos Breling
 */

class BaustoffKategorieToCategoryCopyController extends Controller
{
    /**
     * @Route("/categories")
     */
    public function listAction()
    {


        $em = $this->getDoctrine()->getManager();
        $baustoffKategorienArray = $em->getRepository(BaustoffKategorie::class)->findAll();
        dump($baustoffKategorienArray);

        $categoriesArrayOld = $em->getRepository(Category::class)->findAll();;
        $categoriesArray = [Category::class];
        if (isset($baustoffKategorienArray) && $baustoffKategorienArray != null && !empty($baustoffKategorienArray) ) {
            foreach ($baustoffKategorienArray as $i => $tempBaustoffKategorie) {
                /**@var BaustoffKategorie $tempBaustoffKategorie */
                $tempBaustoffKategorie;
                if (isset($tempBaustoffKategorie) && $tempBaustoffKategorie != null && !empty($tempBaustoffKategorie )) {
                    /**@var Category $tempCategory */
                    $tempCategory = new Category();
                    $tempCategory->setName($tempBaustoffKategorie->getName());
                    $tempCategory->setUidBaustoff($tempBaustoffKategorie->getUid());
                    //todo: search for a Category with the same UidBaustoff, but in  $categoriesArrayOld only one db query,add: getCategory for uidBaustoff Method
                    //todo: txImpKategorienArray has A LOT of depth because Categories have Parent and Sub categories, analyze and optimize


                    $categoriesArray[$i] = $tempCategory;

                    // tells Doctrine you want to (eventually) save the Category (no queries yet)


                    $em->persist($tempCategory);
                }
            }
            // actually executes the queries (i.e. the INSERT query)
            // of course after the loop. only one query
            $em->flush();

        }

        return $this->render('categories/list.html.twig',
            ['categoriesArray' => $categoriesArray
            ]
        );
    }
}