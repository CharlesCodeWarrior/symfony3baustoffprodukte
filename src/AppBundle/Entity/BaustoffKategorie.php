<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BaustoffKategorie
 *
 * @ORM\Table(name="baustoff_kategorie")
 * @ORM\Entity
 */
class BaustoffKategorie
{
    /**
     * @var integer
     *
     * @ORM\Column(name="pid", type="integer", nullable=false)
     */
    private $pid = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name = '';


    /**
     * @var string
     *
     * @ORM\Column(name="bild", type="text", length=65535, nullable=true)
     */
    private $bild;


    /**
     * @var integer
     *
     * @ORM\Column(name="tstamp", type="integer", nullable=false)
     */
    private $tstamp = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="crdate", type="integer", nullable=false)
     */
    private $crdate = '0';


    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="boolean", nullable=false)
     */
    private $deleted = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="hidden", type="boolean", nullable=false)
     */
    private $hidden = '0';


    /**
     * @var integer
     *
     * @ORM\Column(name="t3_origuid", type="integer", nullable=false)
     */
    private $t3Origuid = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="sys_language_uid", type="integer", nullable=false)
     */
    private $sysLanguageUid = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="l10n_parent", type="integer", nullable=false)
     */
    private $l10nParent = '0';



    /**
     * @var integer
     *
     * @ORM\Column(name="produkt", type="integer", nullable=false)
     */
    private $produkt = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="farbe", type="integer", nullable=false)
     */
    private $farbe = '0';



    /**
     * @var integer
     *
     * @ORM\Column(name="uid", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $uid;



    /**
     * Set pid
     *
     * @param integer $pid
     *
     * @return BaustoffKategorie
     */
    public function setPid($pid)
    {
        $this->pid = $pid;

        return $this;
    }

    /**
     * Get pid
     *
     * @return integer
     */
    public function getPid()
    {
        return $this->pid;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return BaustoffKategorie
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }



    /**
     * Set bild
     *
     * @param string $bild
     *
     * @return BaustoffKategorie
     */
    public function setBild($bild)
    {
        $this->bild = $bild;

        return $this;
    }

    /**
     * Get bild
     *
     * @return string
     */
    public function getBild()
    {
        return $this->bild;
    }



    /**
     * Set tstamp
     *
     * @param integer $tstamp
     *
     * @return BaustoffKategorie
     */
    public function setTstamp($tstamp)
    {
        $this->tstamp = $tstamp;

        return $this;
    }

    /**
     * Get tstamp
     *
     * @return integer
     */
    public function getTstamp()
    {
        return $this->tstamp;
    }

    /**
     * Set crdate
     *
     * @param integer $crdate
     *
     * @return BaustoffKategorie
     */
    public function setCrdate($crdate)
    {
        $this->crdate = $crdate;

        return $this;
    }

    /**
     * Get crdate
     *
     * @return integer
     */
    public function getCrdate()
    {
        return $this->crdate;
    }



    /**
     * Set deleted
     *
     * @param boolean $deleted
     *
     * @return BaustoffKategorie
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return boolean
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Set hidden
     *
     * @param boolean $hidden
     *
     * @return BaustoffKategorie
     */
    public function setHidden($hidden)
    {
        $this->hidden = $hidden;

        return $this;
    }

    /**
     * Get hidden
     *
     * @return boolean
     */
    public function getHidden()
    {
        return $this->hidden;
    }


    /**
     * Set t3Origuid
     *
     * @param integer $t3Origuid
     *
     * @return BaustoffKategorie
     */
    public function setT3Origuid($t3Origuid)
    {
        $this->t3Origuid = $t3Origuid;

        return $this;
    }

    /**
     * Get t3Origuid
     *
     * @return integer
     */
    public function getT3Origuid()
    {
        return $this->t3Origuid;
    }

    /**
     * Set sysLanguageUid
     *
     * @param integer $sysLanguageUid
     *
     * @return BaustoffKategorie
     */
    public function setSysLanguageUid($sysLanguageUid)
    {
        $this->sysLanguageUid = $sysLanguageUid;

        return $this;
    }

    /**
     * Get sysLanguageUid
     *
     * @return integer
     */
    public function getSysLanguageUid()
    {
        return $this->sysLanguageUid;
    }

    /**
     * Set l10nParent
     *
     * @param integer $l10nParent
     *
     * @return BaustoffKategorie
     */
    public function setL10nParent($l10nParent)
    {
        $this->l10nParent = $l10nParent;

        return $this;
    }

    /**
     * Get l10nParent
     *
     * @return integer
     */
    public function getL10nParent()
    {
        return $this->l10nParent;
    }


    /**
     * Set produkt
     *
     * @param integer $produkt
     *
     * @return BaustoffKategorie
     */
    public function setProdukt($produkt)
    {
        $this->produkt = $produkt;

        return $this;
    }

    /**
     * Get produkt
     *
     * @return integer
     */
    public function getProdukt()
    {
        return $this->produkt;
    }

    /**
     * Set farbe
     *
     * @param integer $farbe
     *
     * @return BaustoffKategorie
     */
    public function setFarbe($farbe)
    {
        $this->farbe = $farbe;

        return $this;
    }

    /**
     * Get farbe
     *
     * @return integer
     */
    public function getFarbe()
    {
        return $this->farbe;
    }


    /**
     * Get uid
     *
     * @return integer
     */
    public function getUid()
    {
        return $this->uid;
    }
}
