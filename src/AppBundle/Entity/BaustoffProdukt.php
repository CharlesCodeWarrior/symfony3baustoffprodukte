<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *
 *
 * @ORM\Entity
 * @ORM\Table(name="baustoff_produkt", indexes={@ORM\Index(name="parent", columns={"pid"}), @ORM\Index(name="t3ver_oid", columns={"t3ver_oid", "t3ver_wsid"}), @ORM\Index(name="language", columns={"l10n_parent", "sys_language_uid"})})
 */
class BaustoffProdukt
{
    /**
     * @var integer
     *
     * @ORM\Column(name="pid", type="integer", nullable=false)
     */
    private $pid = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name = '';

    /**
     * @var string
     *
     * @ORM\Column(name="bezeichnung", type="string", length=255, nullable=false)
     */
    private $bezeichnung = '';

    /**
     * @var string
     *
     * @ORM\Column(name="eingangstext", type="text", length=65535, nullable=false)
     */
    private $eingangstext;

    /**
     * @var string
     *
     * @ORM\Column(name="bild", type="text", length=65535, nullable=false)
     */
    private $bild;

    /**
     * @var string
     *
     * @ORM\Column(name="eigenschaften", type="text", length=65535, nullable=false)
     */
    private $eigenschaften;

    /**
     * @var string
     *
     * @ORM\Column(name="anwendungsbereiche", type="text", length=65535, nullable=false)
     */
    private $anwendungsbereiche;

    /**
     * @var string
     *
     * @ORM\Column(name="geeigneteuntergruende", type="text", length=65535, nullable=false)
     */
    private $geeigneteuntergruende;





    /**
     * @var string
     *
     * @ORM\Column(name="mischverhaeltnis", type="text", length=65535, nullable=false)
     */
    private $mischverhaeltnis;

    /**
     * @var string
     *
     * @ORM\Column(name="materialdichte", type="text", length=65535, nullable=false)
     */
    private $materialdichte;

    /**
     * @var string
     *
     * @ORM\Column(name="rollenmasse", type="text", length=65535, nullable=false)
     */
    private $rollenmasse;

    /**
     * @var string
     *
     * @ORM\Column(name="raumgewicht", type="text", length=65535, nullable=false)
     */
    private $raumgewicht;

    /**
     * @var string
     *
     * @ORM\Column(name="temperaturbestaendigkeit", type="text", length=65535, nullable=false)
     */
    private $temperaturbestaendigkeit;

    /**
     * @var string
     *
     * @ORM\Column(name="waermedurchlasswiderstand", type="text", length=65535, nullable=false)
     */
    private $waermedurchlasswiderstand;

    /**
     * @var string
     *
     * @ORM\Column(name="trittschallverbesserungsmass", type="text", length=65535, nullable=false)
     */
    private $trittschallverbesserungsmass;

    /**
     * @var string
     *
     * @ORM\Column(name="materialbasis", type="text", length=65535, nullable=false)
     */
    private $materialbasis;

    /**
     * @var string
     *
     * @ORM\Column(name="farbtoene", type="text", length=65535, nullable=false)
     */
    private $farbtoene;



    /**
     * @var string
     *
     * @ORM\Column(name="anmischverhaeltnis", type="text", length=65535, nullable=false)
     */
    private $anmischverhaeltnis;

    /**
     * @var string
     *
     * @ORM\Column(name="optimalerwasserstoffwert", type="text", length=65535, nullable=false)
     */
    private $optimalerwasserstoffwert;

    /**
     * @var string
     *
     * @ORM\Column(name="reifezeit", type="text", length=65535, nullable=false)
     */
    private $reifezeit;

    /**
     * @var string
     *
     * @ORM\Column(name="belegreife", type="text", length=65535, nullable=false)
     */
    private $belegreife;

    /**
     * @var string
     *
     * @ORM\Column(name="einlegezeit", type="text", length=65535, nullable=false)
     */
    private $einlegezeit;

    /**
     * @var string
     *
     * @ORM\Column(name="verarbeitungszeit", type="text", length=65535, nullable=false)
     */
    private $verarbeitungszeit;

    /**
     * @var string
     *
     * @ORM\Column(name="schichtdicke", type="text", length=65535, nullable=false)
     */
    private $schichtdicke;

    /**
     * @var string
     *
     * @ORM\Column(name="maxauftragsstaerke", type="text", length=65535, nullable=false)
     */
    private $maxauftragsstaerke;

    /**
     * @var string
     *
     * @ORM\Column(name="erforderlicheschichtstaerken", type="text", length=65535, nullable=false)
     */
    private $erforderlicheschichtstaerken;

    /**
     * @var string
     *
     * @ORM\Column(name="begehbar", type="text", length=65535, nullable=false)
     */
    private $begehbar;

    /**
     * @var string
     *
     * @ORM\Column(name="chemischundmechanischbelastbar", type="text", length=65535, nullable=false)
     */
    private $chemischundmechanischbelastbar;

    /**
     * @var string
     *
     * @ORM\Column(name="mechanischbelastbar", type="text", length=65535, nullable=false)
     */
    private $mechanischbelastbar;

    /**
     * @var string
     *
     * @ORM\Column(name="chemischbelastbar", type="text", length=65535, nullable=false)
     */
    private $chemischbelastbar;

    /**
     * @var string
     *
     * @ORM\Column(name="vollbelastbar", type="text", length=65535, nullable=false)
     */
    private $vollbelastbar;

    /**
     * @var string
     *
     * @ORM\Column(name="belastbar", type="text", length=65535, nullable=false)
     */
    private $belastbar;

    /**
     * @var string
     *
     * @ORM\Column(name="verfugbar", type="text", length=65535, nullable=false)
     */
    private $verfugbar;

    /**
     * @var string
     *
     * @ORM\Column(name="schleifbar", type="text", length=65535, nullable=false)
     */
    private $schleifbar;

    /**
     * @var string
     *
     * @ORM\Column(name="verarbeitungsbedingungen", type="text", length=65535, nullable=false)
     */
    private $verarbeitungsbedingungen;

    /**
     * @var string
     *
     * @ORM\Column(name="verarbeitungstemperatur", type="text", length=65535, nullable=false)
     */
    private $verarbeitungstemperatur;

    /**
     * @var string
     *
     * @ORM\Column(name="verarbeitungsunduntergrundtemperatur", type="text", length=65535, nullable=false)
     */
    private $verarbeitungsunduntergrundtemperatur;


    /**
     * @var string
     *
     * @ORM\Column(name="trocknungszeit", type="text", length=65535, nullable=false)
     */
    private $trocknungszeit;

    /**
     * @var string
     *
     * @ORM\Column(name="aushaertezeit", type="text", length=65535, nullable=false)
     */
    private $aushaertezeit;

    /**
     * @var string
     *
     * @ORM\Column(name="zeitabstandzwischendeneinzelnenauftraegen", type="text", length=65535, nullable=false)
     */
    private $zeitabstandzwischendeneinzelnenauftraegen;

    /**
     * @var string
     *
     * @ORM\Column(name="zeitabstandzwischenletztemauftragundverlegen", type="text", length=65535, nullable=false)
     */
    private $zeitabstandzwischenletztemauftragundverlegen;

    /**
     * @var string
     *
     * @ORM\Column(name="kategorien", type="string", length=255, nullable=false)
     */
    private $kategorien = '';


    /**
     * @var integer
     *
     * @ORM\Column(name="tstamp", type="integer", nullable=false)
     */
    private $tstamp = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="crdate", type="integer", nullable=false)
     */
    private $crdate = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="cruser_id", type="integer", nullable=false)
     */
    private $cruserId = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="boolean", nullable=false)
     */
    private $deleted = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="hidden", type="boolean", nullable=false)
     */
    private $hidden = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="starttime", type="integer", nullable=false)
     */
    private $starttime = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="endtime", type="integer", nullable=false)
     */
    private $endtime = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="t3ver_oid", type="integer", nullable=false)
     */
    private $t3verOid = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="t3ver_id", type="integer", nullable=false)
     */
    private $t3verId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="t3ver_wsid", type="integer", nullable=false)
     */
    private $t3verWsid = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="t3ver_label", type="string", length=255, nullable=false)
     */
    private $t3verLabel = '';

    /**
     * @var boolean
     *
     * @ORM\Column(name="t3ver_state", type="boolean", nullable=false)
     */
    private $t3verState = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="t3ver_stage", type="integer", nullable=false)
     */
    private $t3verStage = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="t3ver_count", type="integer", nullable=false)
     */
    private $t3verCount = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="t3ver_tstamp", type="integer", nullable=false)
     */
    private $t3verTstamp = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="t3ver_move_id", type="integer", nullable=false)
     */
    private $t3verMoveId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="sorting", type="integer", nullable=false)
     */
    private $sorting = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="t3_origuid", type="integer", nullable=false)
     */
    private $t3Origuid = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="sys_language_uid", type="integer", nullable=false)
     */
    private $sysLanguageUid = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="l10n_parent", type="integer", nullable=false)
     */
    private $l10nParent = '0';



    /**
     * @var string
     *
     * @ORM\Column(name="verarbeitung", type="text", length=65535, nullable=false)
     */
    private $verarbeitung;


    /**
     * @var integer
     *
     * @ORM\Column(name="ausblenden", type="integer", nullable=false)
     */
    private $ausblenden = '0';



    /**
     * @var integer
     *
     * @ORM\Column(name="uid", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $uid;



    /**
     * Set pid
     *
     * @param integer $pid
     *
     * @return BaustoffProdukt
     */
    public function setPid($pid)
    {
        $this->pid = $pid;

        return $this;
    }

    /**
     * Get pid
     *
     * @return integer
     */
    public function getPid()
    {
        return $this->pid;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return BaustoffProdukt
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set bezeichnung
     *
     * @param string $bezeichnung
     *
     * @return BaustoffProdukt
     */
    public function setBezeichnung($bezeichnung)
    {
        $this->bezeichnung = $bezeichnung;

        return $this;
    }

    /**
     * Get bezeichnung
     *
     * @return string
     */
    public function getBezeichnung()
    {
        return $this->bezeichnung;
    }

    /**
     * Set eingangstext
     *
     * @param string $eingangstext
     *
     * @return BaustoffProdukt
     */
    public function setEingangstext($eingangstext)
    {
        $this->eingangstext = $eingangstext;

        return $this;
    }

    /**
     * Get eingangstext
     *
     * @return string
     */
    public function getEingangstext()
    {
        return $this->eingangstext;
    }

    /**
     * Set bild
     *
     * @param string $bild
     *
     * @return BaustoffProdukt
     */
    public function setBild($bild)
    {
        $this->bild = $bild;

        return $this;
    }

    /**
     * Get bild
     *
     * @return string
     */
    public function getBild()
    {
        return $this->bild;
    }

    /**
     * Set eigenschaften
     *
     * @param string $eigenschaften
     *
     * @return BaustoffProdukt
     */
    public function setEigenschaften($eigenschaften)
    {
        $this->eigenschaften = $eigenschaften;

        return $this;
    }

    /**
     * Get eigenschaften
     *
     * @return string
     */
    public function getEigenschaften()
    {
        return $this->eigenschaften;
    }

    /**
     * Set anwendungsbereiche
     *
     * @param string $anwendungsbereiche
     *
     * @return BaustoffProdukt
     */
    public function setAnwendungsbereiche($anwendungsbereiche)
    {
        $this->anwendungsbereiche = $anwendungsbereiche;

        return $this;
    }

    /**
     * Get anwendungsbereiche
     *
     * @return string
     */
    public function getAnwendungsbereiche()
    {
        return $this->anwendungsbereiche;
    }

    /**
     * Set geeigneteuntergruende
     *
     * @param string $geeigneteuntergruende
     *
     * @return BaustoffProdukt
     */
    public function setGeeigneteuntergruende($geeigneteuntergruende)
    {
        $this->geeigneteuntergruende = $geeigneteuntergruende;

        return $this;
    }

    /**
     * Get geeigneteuntergruende
     *
     * @return string
     */
    public function getGeeigneteuntergruende()
    {
        return $this->geeigneteuntergruende;
    }




    public function setMischverhaeltnis($mischverhaeltnis)
    {
        $this->mischverhaeltnis = $mischverhaeltnis;

        return $this;
    }

    /**
     * Get mischverhaeltnis
     *
     * @return string
     */
    public function getMischverhaeltnis()
    {
        return $this->mischverhaeltnis;
    }

    /**
     * Set materialdichte
     *
     * @param string $materialdichte
     *
     * @return BaustoffProdukt
     */
    public function setMaterialdichte($materialdichte)
    {
        $this->materialdichte = $materialdichte;

        return $this;
    }

    /**
     * Get materialdichte
     *
     * @return string
     */
    public function getMaterialdichte()
    {
        return $this->materialdichte;
    }

    /**
     * Set rollenmasse
     *
     * @param string $rollenmasse
     *
     * @return BaustoffProdukt
     */
    public function setRollenmasse($rollenmasse)
    {
        $this->rollenmasse = $rollenmasse;

        return $this;
    }

    /**
     * Get rollenmasse
     *
     * @return string
     */
    public function getRollenmasse()
    {
        return $this->rollenmasse;
    }

    /**
     * Set raumgewicht
     *
     * @param string $raumgewicht
     *
     * @return BaustoffProdukt
     */
    public function setRaumgewicht($raumgewicht)
    {
        $this->raumgewicht = $raumgewicht;

        return $this;
    }

    /**
     * Get raumgewicht
     *
     * @return string
     */
    public function getRaumgewicht()
    {
        return $this->raumgewicht;
    }

    /**
     * Set temperaturbestaendigkeit
     *
     * @param string $temperaturbestaendigkeit
     *
     * @return BaustoffProdukt
     */
    public function setTemperaturbestaendigkeit($temperaturbestaendigkeit)
    {
        $this->temperaturbestaendigkeit = $temperaturbestaendigkeit;

        return $this;
    }

    /**
     * Get temperaturbestaendigkeit
     *
     * @return string
     */
    public function getTemperaturbestaendigkeit()
    {
        return $this->temperaturbestaendigkeit;
    }

    /**
     * Set waermedurchlasswiderstand
     *
     * @param string $waermedurchlasswiderstand
     *
     * @return BaustoffProdukt
     */
    public function setWaermedurchlasswiderstand($waermedurchlasswiderstand)
    {
        $this->waermedurchlasswiderstand = $waermedurchlasswiderstand;

        return $this;
    }

    /**
     * Get waermedurchlasswiderstand
     *
     * @return string
     */
    public function getWaermedurchlasswiderstand()
    {
        return $this->waermedurchlasswiderstand;
    }

    /**
     * Set trittschallverbesserungsmass
     *
     * @param string $trittschallverbesserungsmass
     *
     * @return BaustoffProdukt
     */
    public function setTrittschallverbesserungsmass($trittschallverbesserungsmass)
    {
        $this->trittschallverbesserungsmass = $trittschallverbesserungsmass;

        return $this;
    }

    /**
     * Get trittschallverbesserungsmass
     *
     * @return string
     */
    public function getTrittschallverbesserungsmass()
    {
        return $this->trittschallverbesserungsmass;
    }

    /**
     * Set materialbasis
     *
     * @param string $materialbasis
     *
     * @return BaustoffProdukt
     */
    public function setMaterialbasis($materialbasis)
    {
        $this->materialbasis = $materialbasis;

        return $this;
    }

    /**
     * Get materialbasis
     *
     * @return string
     */
    public function getMaterialbasis()
    {
        return $this->materialbasis;
    }

    /**
     * Set farbtoene
     *
     * @param string $farbtoene
     *
     * @return BaustoffProdukt
     */
    public function setFarbtoene($farbtoene)
    {
        $this->farbtoene = $farbtoene;

        return $this;
    }

    /**
     * Get farbtoene
     *
     * @return string
     */
    public function getFarbtoene()
    {
        return $this->farbtoene;
    }



    /**
     * Set anmischverhaeltnis
     *
     * @param string $anmischverhaeltnis
     *
     * @return BaustoffProdukt
     */
    public function setAnmischverhaeltnis($anmischverhaeltnis)
    {
        $this->anmischverhaeltnis = $anmischverhaeltnis;

        return $this;
    }

    /**
     * Get anmischverhaeltnis
     *
     * @return string
     */
    public function getAnmischverhaeltnis()
    {
        return $this->anmischverhaeltnis;
    }

    /**
     * Set optimalerwasserstoffwert
     *
     * @param string $optimalerwasserstoffwert
     *
     * @return BaustoffProdukt
     */
    public function setOptimalerwasserstoffwert($optimalerwasserstoffwert)
    {
        $this->optimalerwasserstoffwert = $optimalerwasserstoffwert;

        return $this;
    }

    /**
     * Get optimalerwasserstoffwert
     *
     * @return string
     */
    public function getOptimalerwasserstoffwert()
    {
        return $this->optimalerwasserstoffwert;
    }

    /**
     * Set reifezeit
     *
     * @param string $reifezeit
     *
     * @return BaustoffProdukt
     */
    public function setReifezeit($reifezeit)
    {
        $this->reifezeit = $reifezeit;

        return $this;
    }

    /**
     * Get reifezeit
     *
     * @return string
     */
    public function getReifezeit()
    {
        return $this->reifezeit;
    }

    /**
     * Set belegreife
     *
     * @param string $belegreife
     *
     * @return BaustoffProdukt
     */
    public function setBelegreife($belegreife)
    {
        $this->belegreife = $belegreife;

        return $this;
    }

    /**
     * Get belegreife
     *
     * @return string
     */
    public function getBelegreife()
    {
        return $this->belegreife;
    }

    /**
     * Set einlegezeit
     *
     * @param string $einlegezeit
     *
     * @return BaustoffProdukt
     */
    public function setEinlegezeit($einlegezeit)
    {
        $this->einlegezeit = $einlegezeit;

        return $this;
    }

    /**
     * Get einlegezeit
     *
     * @return string
     */
    public function getEinlegezeit()
    {
        return $this->einlegezeit;
    }

    /**
     * Set verarbeitungszeit
     *
     * @param string $verarbeitungszeit
     *
     * @return BaustoffProdukt
     */
    public function setVerarbeitungszeit($verarbeitungszeit)
    {
        $this->verarbeitungszeit = $verarbeitungszeit;

        return $this;
    }

    /**
     * Get verarbeitungszeit
     *
     * @return string
     */
    public function getVerarbeitungszeit()
    {
        return $this->verarbeitungszeit;
    }

    /**
     * Set schichtdicke
     *
     * @param string $schichtdicke
     *
     * @return BaustoffProdukt
     */
    public function setSchichtdicke($schichtdicke)
    {
        $this->schichtdicke = $schichtdicke;

        return $this;
    }

    /**
     * Get schichtdicke
     *
     * @return string
     */
    public function getSchichtdicke()
    {
        return $this->schichtdicke;
    }

    /**
     * Set maxauftragsstaerke
     *
     * @param string $maxauftragsstaerke
     *
     * @return BaustoffProdukt
     */
    public function setMaxauftragsstaerke($maxauftragsstaerke)
    {
        $this->maxauftragsstaerke = $maxauftragsstaerke;

        return $this;
    }

    /**
     * Get maxauftragsstaerke
     *
     * @return string
     */
    public function getMaxauftragsstaerke()
    {
        return $this->maxauftragsstaerke;
    }

    /**
     * Set erforderlicheschichtstaerken
     *
     * @param string $erforderlicheschichtstaerken
     *
     * @return BaustoffProdukt
     */
    public function setErforderlicheschichtstaerken($erforderlicheschichtstaerken)
    {
        $this->erforderlicheschichtstaerken = $erforderlicheschichtstaerken;

        return $this;
    }

    /**
     * Get erforderlicheschichtstaerken
     *
     * @return string
     */
    public function getErforderlicheschichtstaerken()
    {
        return $this->erforderlicheschichtstaerken;
    }

    /**
     * Set begehbar
     *
     * @param string $begehbar
     *
     * @return BaustoffProdukt
     */
    public function setBegehbar($begehbar)
    {
        $this->begehbar = $begehbar;

        return $this;
    }

    /**
     * Get begehbar
     *
     * @return string
     */
    public function getBegehbar()
    {
        return $this->begehbar;
    }

    /**
     * Set chemischundmechanischbelastbar
     *
     * @param string $chemischundmechanischbelastbar
     *
     * @return BaustoffProdukt
     */
    public function setChemischundmechanischbelastbar($chemischundmechanischbelastbar)
    {
        $this->chemischundmechanischbelastbar = $chemischundmechanischbelastbar;

        return $this;
    }

    /**
     * Get chemischundmechanischbelastbar
     *
     * @return string
     */
    public function getChemischundmechanischbelastbar()
    {
        return $this->chemischundmechanischbelastbar;
    }

    /**
     * Set mechanischbelastbar
     *
     * @param string $mechanischbelastbar
     *
     * @return BaustoffProdukt
     */
    public function setMechanischbelastbar($mechanischbelastbar)
    {
        $this->mechanischbelastbar = $mechanischbelastbar;

        return $this;
    }

    /**
     * Get mechanischbelastbar
     *
     * @return string
     */
    public function getMechanischbelastbar()
    {
        return $this->mechanischbelastbar;
    }

    /**
     * Set chemischbelastbar
     *
     * @param string $chemischbelastbar
     *
     * @return BaustoffProdukt
     */
    public function setChemischbelastbar($chemischbelastbar)
    {
        $this->chemischbelastbar = $chemischbelastbar;

        return $this;
    }

    /**
     * Get chemischbelastbar
     *
     * @return string
     */
    public function getChemischbelastbar()
    {
        return $this->chemischbelastbar;
    }

    /**
     * Set vollbelastbar
     *
     * @param string $vollbelastbar
     *
     * @return BaustoffProdukt
     */
    public function setVollbelastbar($vollbelastbar)
    {
        $this->vollbelastbar = $vollbelastbar;

        return $this;
    }

    /**
     * Get vollbelastbar
     *
     * @return string
     */
    public function getVollbelastbar()
    {
        return $this->vollbelastbar;
    }

    /**
     * Set belastbar
     *
     * @param string $belastbar
     *
     * @return BaustoffProdukt
     */
    public function setBelastbar($belastbar)
    {
        $this->belastbar = $belastbar;

        return $this;
    }

    /**
     * Get belastbar
     *
     * @return string
     */
    public function getBelastbar()
    {
        return $this->belastbar;
    }

    /**
     * Set verfugbar
     *
     * @param string $verfugbar
     *
     * @return BaustoffProdukt
     */
    public function setVerfugbar($verfugbar)
    {
        $this->verfugbar = $verfugbar;

        return $this;
    }

    /**
     * Get verfugbar
     *
     * @return string
     */
    public function getVerfugbar()
    {
        return $this->verfugbar;
    }

    /**
     * Set schleifbar
     *
     * @param string $schleifbar
     *
     * @return BaustoffProdukt
     */
    public function setSchleifbar($schleifbar)
    {
        $this->schleifbar = $schleifbar;

        return $this;
    }

    /**
     * Get schleifbar
     *
     * @return string
     */
    public function getSchleifbar()
    {
        return $this->schleifbar;
    }

    /**
     * Set verarbeitungsbedingungen
     *
     * @param string $verarbeitungsbedingungen
     *
     * @return BaustoffProdukt
     */
    public function setVerarbeitungsbedingungen($verarbeitungsbedingungen)
    {
        $this->verarbeitungsbedingungen = $verarbeitungsbedingungen;

        return $this;
    }

    /**
     * Get verarbeitungsbedingungen
     *
     * @return string
     */
    public function getVerarbeitungsbedingungen()
    {
        return $this->verarbeitungsbedingungen;
    }

    /**
     * Set verarbeitungstemperatur
     *
     * @param string $verarbeitungstemperatur
     *
     * @return BaustoffProdukt
     */
    public function setVerarbeitungstemperatur($verarbeitungstemperatur)
    {
        $this->verarbeitungstemperatur = $verarbeitungstemperatur;

        return $this;
    }

    /**
     * Get verarbeitungstemperatur
     *
     * @return string
     */
    public function getVerarbeitungstemperatur()
    {
        return $this->verarbeitungstemperatur;
    }

    /**
     * Set verarbeitungsunduntergrundtemperatur
     *
     * @param string $verarbeitungsunduntergrundtemperatur
     *
     * @return BaustoffProdukt
     */
    public function setVerarbeitungsunduntergrundtemperatur($verarbeitungsunduntergrundtemperatur)
    {
        $this->verarbeitungsunduntergrundtemperatur = $verarbeitungsunduntergrundtemperatur;

        return $this;
    }

    /**
     * Get verarbeitungsunduntergrundtemperatur
     *
     * @return string
     */
    public function getVerarbeitungsunduntergrundtemperatur()
    {
        return $this->verarbeitungsunduntergrundtemperatur;
    }



    /**
     * Set trocknungszeit
     *
     * @param string $trocknungszeit
     *
     * @return BaustoffProdukt
     */
    public function setTrocknungszeit($trocknungszeit)
    {
        $this->trocknungszeit = $trocknungszeit;

        return $this;
    }

    /**
     * Get trocknungszeit
     *
     * @return string
     */
    public function getTrocknungszeit()
    {
        return $this->trocknungszeit;
    }

    /**
     * Set aushaertezeit
     *
     * @param string $aushaertezeit
     *
     * @return BaustoffProdukt
     */
    public function setAushaertezeit($aushaertezeit)
    {
        $this->aushaertezeit = $aushaertezeit;

        return $this;
    }

    /**
     * Get aushaertezeit
     *
     * @return string
     */
    public function getAushaertezeit()
    {
        return $this->aushaertezeit;
    }

    /**
     * Set zeitabstandzwischendeneinzelnenauftraegen
     *
     * @param string $zeitabstandzwischendeneinzelnenauftraegen
     *
     * @return BaustoffProdukt
     */
    public function setZeitabstandzwischendeneinzelnenauftraegen($zeitabstandzwischendeneinzelnenauftraegen)
    {
        $this->zeitabstandzwischendeneinzelnenauftraegen = $zeitabstandzwischendeneinzelnenauftraegen;

        return $this;
    }

    /**
     * Get zeitabstandzwischendeneinzelnenauftraegen
     *
     * @return string
     */
    public function getZeitabstandzwischendeneinzelnenauftraegen()
    {
        return $this->zeitabstandzwischendeneinzelnenauftraegen;
    }

    /**
     * Set zeitabstandzwischenletztemauftragundverlegen
     *
     * @param string $zeitabstandzwischenletztemauftragundverlegen
     *
     * @return BaustoffProdukt
     */
    public function setZeitabstandzwischenletztemauftragundverlegen($zeitabstandzwischenletztemauftragundverlegen)
    {
        $this->zeitabstandzwischenletztemauftragundverlegen = $zeitabstandzwischenletztemauftragundverlegen;

        return $this;
    }

    /**
     * Get zeitabstandzwischenletztemauftragundverlegen
     *
     * @return string
     */
    public function getZeitabstandzwischenletztemauftragundverlegen()
    {
        return $this->zeitabstandzwischenletztemauftragundverlegen;
    }

    /**
     * Set kategorien
     *
     * @param string $kategorien
     *
     * @return BaustoffProdukt
     */
    public function setKategorien($kategorien)
    {
        $this->kategorien = $kategorien;

        return $this;
    }

    /**
     * Get kategorien
     *
     * @return string
     */
    public function getKategorien()
    {
        return $this->kategorien;
    }



    /**
     * Get tstamp
     *
     * @return integer
     */
    public function getTstamp()
    {
        return $this->tstamp;
    }

    /**
     * Set crdate
     *
     * @param integer $crdate
     *
     * @return BaustoffProdukt
     */
    public function setCrdate($crdate)
    {
        $this->crdate = $crdate;

        return $this;
    }

    /**
     * Get crdate
     *
     * @return integer
     */
    public function getCrdate()
    {
        return $this->crdate;
    }

    /**
     * Set cruserId
     *
     * @param integer $cruserId
     *
     * @return BaustoffProdukt
     */
    public function setCruserId($cruserId)
    {
        $this->cruserId = $cruserId;

        return $this;
    }

    /**
     * Get cruserId
     *
     * @return integer
     */
    public function getCruserId()
    {
        return $this->cruserId;
    }

    /**
     * Set deleted
     *
     * @param boolean $deleted
     *
     * @return BaustoffProdukt
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return boolean
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Set hidden
     *
     * @param boolean $hidden
     *
     * @return BaustoffProdukt
     */
    public function setHidden($hidden)
    {
        $this->hidden = $hidden;

        return $this;
    }

    /**
     * Get hidden
     *
     * @return boolean
     */
    public function getHidden()
    {
        return $this->hidden;
    }

    /**
     * Set starttime
     *
     * @param integer $starttime
     *
     * @return BaustoffProdukt
     */
    public function setStarttime($starttime)
    {
        $this->starttime = $starttime;

        return $this;
    }

    /**
     * Get starttime
     *
     * @return integer
     */
    public function getStarttime()
    {
        return $this->starttime;
    }

    /**
     * Set endtime
     *
     * @param integer $endtime
     *
     * @return BaustoffProdukt
     */
    public function setEndtime($endtime)
    {
        $this->endtime = $endtime;

        return $this;
    }

    /**
     * Get endtime
     *
     * @return integer
     */
    public function getEndtime()
    {
        return $this->endtime;
    }

    /**
     * Set t3verOid
     *
     * @param integer $t3verOid
     *
     * @return BaustoffProdukt
     */
    public function setT3verOid($t3verOid)
    {
        $this->t3verOid = $t3verOid;

        return $this;
    }

    /**
     * Get t3verOid
     *
     * @return integer
     */
    public function getT3verOid()
    {
        return $this->t3verOid;
    }

    /**
     * Set t3verId
     *
     * @param integer $t3verId
     *
     * @return BaustoffProdukt
     */
    public function setT3verId($t3verId)
    {
        $this->t3verId = $t3verId;

        return $this;
    }

    /**
     * Get t3verId
     *
     * @return integer
     */
    public function getT3verId()
    {
        return $this->t3verId;
    }

    /**
     * Set t3verWsid
     *
     * @param integer $t3verWsid
     *
     * @return BaustoffProdukt
     */
    public function setT3verWsid($t3verWsid)
    {
        $this->t3verWsid = $t3verWsid;

        return $this;
    }

    /**
     * Get t3verWsid
     *
     * @return integer
     */
    public function getT3verWsid()
    {
        return $this->t3verWsid;
    }

    /**
     * Set t3verLabel
     *
     * @param string $t3verLabel
     *
     * @return BaustoffProdukt
     */
    public function setT3verLabel($t3verLabel)
    {
        $this->t3verLabel = $t3verLabel;

        return $this;
    }

    /**
     * Get t3verLabel
     *
     * @return string
     */
    public function getT3verLabel()
    {
        return $this->t3verLabel;
    }

    /**
     * Set t3verState
     *
     * @param boolean $t3verState
     *
     * @return BaustoffProdukt
     */
    public function setT3verState($t3verState)
    {
        $this->t3verState = $t3verState;

        return $this;
    }

    /**
     * Get t3verState
     *
     * @return boolean
     */
    public function getT3verState()
    {
        return $this->t3verState;
    }

    /**
     * Set t3verStage
     *
     * @param integer $t3verStage
     *
     * @return BaustoffProdukt
     */
    public function setT3verStage($t3verStage)
    {
        $this->t3verStage = $t3verStage;

        return $this;
    }

    /**
     * Get t3verStage
     *
     * @return integer
     */
    public function getT3verStage()
    {
        return $this->t3verStage;
    }

    /**
     * Set t3verCount
     *
     * @param integer $t3verCount
     *
     * @return BaustoffProdukt
     */
    public function setT3verCount($t3verCount)
    {
        $this->t3verCount = $t3verCount;

        return $this;
    }

    /**
     * Get t3verCount
     *
     * @return integer
     */
    public function getT3verCount()
    {
        return $this->t3verCount;
    }

    /**
     * Set t3verTstamp
     *
     * @param integer $t3verTstamp
     *
     * @return BaustoffProdukt
     */
    public function setT3verTstamp($t3verTstamp)
    {
        $this->t3verTstamp = $t3verTstamp;

        return $this;
    }

    /**
     * Get t3verTstamp
     *
     * @return integer
     */
    public function getT3verTstamp()
    {
        return $this->t3verTstamp;
    }

    /**
     * Set t3verMoveId
     *
     * @param integer $t3verMoveId
     *
     * @return BaustoffProdukt
     */
    public function setT3verMoveId($t3verMoveId)
    {
        $this->t3verMoveId = $t3verMoveId;

        return $this;
    }

    /**
     * Get t3verMoveId
     *
     * @return integer
     */
    public function getT3verMoveId()
    {
        return $this->t3verMoveId;
    }

    /**
     * Set sorting
     *
     * @param integer $sorting
     *
     * @return BaustoffProdukt
     */
    public function setSorting($sorting)
    {
        $this->sorting = $sorting;

        return $this;
    }

    /**
     * Get sorting
     *
     * @return integer
     */
    public function getSorting()
    {
        return $this->sorting;
    }

    /**
     * Set t3Origuid
     *
     * @param integer $t3Origuid
     *
     * @return BaustoffProdukt
     */
    public function setT3Origuid($t3Origuid)
    {
        $this->t3Origuid = $t3Origuid;

        return $this;
    }

    /**
     * Get t3Origuid
     *
     * @return integer
     */
    public function getT3Origuid()
    {
        return $this->t3Origuid;
    }

    /**
     * Set sysLanguageUid
     *
     * @param integer $sysLanguageUid
     *
     * @return BaustoffProdukt
     */
    public function setSysLanguageUid($sysLanguageUid)
    {
        $this->sysLanguageUid = $sysLanguageUid;

        return $this;
    }

    /**
     * Get sysLanguageUid
     *
     * @return integer
     */
    public function getSysLanguageUid()
    {
        return $this->sysLanguageUid;
    }

    /**
     * Set l10nParent
     *
     * @param integer $l10nParent
     *
     * @return BaustoffProdukt
     */
    public function setL10nParent($l10nParent)
    {
        $this->l10nParent = $l10nParent;

        return $this;
    }

    /**
     * Get l10nParent
     *
     * @return integer
     */
    public function getL10nParent()
    {
        return $this->l10nParent;
    }



    /**
     * Set verarbeitung
     *
     * @param string $verarbeitung
     *
     * @return BaustoffProdukt
     */
    public function setVerarbeitung($verarbeitung)
    {
        $this->verarbeitung = $verarbeitung;

        return $this;
    }

    /**
     * Get verarbeitung
     *
     * @return string
     */
    public function getVerarbeitung()
    {
        return $this->verarbeitung;
    }


    public function setAusblenden($ausblenden)
    {
        $this->ausblenden = $ausblenden;

        return $this;
    }

    /**
     * Get ausblenden
     *
     * @return integer
     */
    public function getAusblenden()
    {
        return $this->ausblenden;
    }


    /**
     * Get uid
     *
     * @return integer
     */
    public function getUid()
    {
        return $this->uid;
    }
}
