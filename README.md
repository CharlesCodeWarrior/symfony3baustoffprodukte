This Symfony3 Project imports BaustoffProdukte from a typo3-mysql-database into a new 'Product'-structure. The same for Baustoff-Kategorie and Categories. n to m relation before , solved by a mapping table.

Dieses  Symfony3 Projekt importiert BaustoffProdukte aus einer typo3-mysql-database uin eine neue 'Product'-Struktur. Dasselbe passiert mit BauStoffKategorien und 'Categories'. Es bestand eine n zu m Beziehung zwischen beiden, diese wurde mit einer Mapping Tabelle aufgelösrt. Doctrine und ORM werden verwendet.

Installation: 
git clone,  
composer install,   
phpmyadmin import db from DUMP